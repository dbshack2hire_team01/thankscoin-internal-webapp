'use strict';

angular.module('myApp.dashboard')

    .config(['$routeProvider', function($routeProvider) {
        $routeProvider.when('/account', {
            templateUrl: 'account/account.view.html',
            controller: 'accountCtrl'
        });


    }])

    .controller('accountCtrl',['$rootScope', '$location', '$http','$scope','config','$location',function($rootScope,$location,$http,$scope,config,location) {

        $scope.createAccount =function () {
            $scope.dataLoading = true;
            var account_id= $('#coinNo').val();
            var account_description = $('#account-description').val();
            console.log(account_description,account_id);
            $('.modal').modal('hide');
            $scope.dataLoading = false;
            $http.post(config.apiUrl+'/accounts/create', { amount: account_id, description: account_description }).then(handleSuccessAccount, handleErrAccount);
        };


        function handleSuccessAccount(res) {
            $('.modal').modal('hide');
            $location.path('/account');
            $scope.dataLoading = false;
        }

        function handleErrAccount(res) {
            $('.modal').modal('hide');
            $('.fade').modal('hide');
            alert('Account already exist ');
        }

        // $scope.queryAccount =function () {
        //     $scope.dataLoading = true;
        //     var account_id= $('#account-id').val();
        //     var account_description = $('#account-description').val();
        //     console.log(account_description,account_id)
        //
        //     $http.post(config.apiUrl+'/accounts/create', { amount: account_id, description: account_description }).then(handleSuccessAccount, handleErrAccount);
        //     $('.modal').modal('hide');
        // };



    }]);
