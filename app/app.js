'use strict';

angular.module('myApp.login', []);
angular.module('myApp.dashboard', []);

angular.module('myApp', [
  'ngRoute',
  'myApp.login',
  'myApp.dashboard',
  'myApp.version'
])
    .constant('config', {
        apiUrl: '//h.r2.io/wallet'
       })

    .config(['$locationProvider', '$routeProvider', function($locationProvider, $routeProvider) {

  $routeProvider.otherwise({redirectTo: '/login'});
}])

.run(['$rootScope', '$location', '$http',
    function ($rootScope, $location, $http) {

        $rootScope.$on('$locationChangeStart', function (event, next, current) {
            // if ($location.path() !== '/login' && !$rootScope.globals.currentUser) {
            //     window.location.href = "/login";
            // }
        });
    }])

    .directive('footer', function () {
        return {
            restrict: 'A',
            replace: true,
            templateUrl: "common/footer.view.html",
            controller: 'footCtrl',
        }
    })

    .directive('header', function () {
        return {
            restrict: 'A',
            replace: true,
            templateUrl: "common/header.view.html",
            controller: 'HeaderController',
        }
    })
