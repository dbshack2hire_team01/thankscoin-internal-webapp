'use strict';

angular.module('myApp')

    .config(['$routeProvider', function($routeProvider) {
        $routeProvider.when('/login', {
            templateUrl: 'user/login.view.html',
            controller: 'userCtrl'
        });

        $routeProvider.when('/register', {
            templateUrl: 'user/register.view.html',
            controller: 'userCtrl'
        });

        $routeProvider.when('/dashboard', {
            templateUrl: 'common/dashboard.view.html',
            controller: 'dashboardCtrl'
        });
    }])

    .controller('userCtrl',['$rootScope','$location', '$scope','$http', 'config', function($rootScope,$location, $scope, $http, config) {
        $scope.dataLoading = false;

        $scope.login =function () {
            $scope.dataLoading = true;
            var email= $('#email').val();
            var password= $('#password').val();
            $rootScope.globalemail = email;
            $http.post(config.apiUrl+'/users/login', { email: email, password: password }).then(handleSuccessLogin, handleErrLogin);
            };


        function handleSuccessLogin(res) {
            // console.log("token>>"+ JSON.stringify(res));

            $scope.dataLoading = false;
            $http.defaults.headers.common['Authorization'] = res.data.token;
            // console.log("token>>"+  $http.defaults.headers.common['Authorization']);
            $rootScope.curr = $rootScope.globals;
            console.log($rootScope.curr);
            $location.path('/dashboard');

        }

        function handleErrLogin(res) {
            alert('password or email not correct ');
        }

        $scope.register =function () {
            $scope.dataLoading = true;
            var regemail= $('#reemail').val();
            var regpassword= $('#repassword').val();
            $http.post(config.apiUrl+'/users/register', { email: regemail, password: regpassword }).then(handleSuccess, handleErr);
            };

            function handleSuccess(res) {
                $rootScope.accountNo = res.data.accountNo;
                $rootScope.globals = {
                    currentUser: {
                        email: res.data.email,
                        accountNo: res.data.accountNo,
                        accountBalance: res.data.accountBalance,
                    }
                };

                console.log($rootScope.globals);
                $scope.dataLoading = false;
                $location.path('/login');

            }

            function handleErr(res) {
                alert('account already exist ');
            }

    }]);
